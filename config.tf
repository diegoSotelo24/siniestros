variable "prefix" {
  description = "Prefijo para todos los recursos que se crearan"
}
variable "azure_location" {
    description = "Ubicacion en donde se desplegaran los recursos"
}

#DATOS CREADOS COMO RBAC
variable "client_id" {
    description = "Client ID"
}
variable "client_secret" {
    description = "Client Secret"
}

#DATOS DE LA SUBSCRIPCION
variable "subscription_id" {
    description = "ID de la suscripcion"
}
variable "tenant_id" {
    description = "ID del tenant"
}

#DATOS PARA LA RED VIRTUAL
variable "virtual_network_name" {
  description = "Nombre de la red virtual"
  default     = "aksVirtualNetwork"
}
variable "virtual_network_address_prefix" {
  description = "RANGO DE IP"
  default     = "15.0.0.0/8"
}

#DATOS PARA SUBNET DE KUBERNETES
variable "aks_subnet_name" {
  description = "AKS Subnet Name."
  default     = "kubesubnet"
}
variable "aks_subnet_address_prefix" {
  description = "SEGMENTO DE RED PARA KUBERNETES"
  default     = "15.240.0.0/16"
}

#DATOS PARA SUBNET DE GATEWAY
variable "aks_subnetGW_name" {
  description = "Gateway Subnet Name."
  default     = "appgwsubnet"
}
variable "app_gateway_subnet_address_prefix" {
  description = "SEGMENTO DE RED PARA EL GATEWAY"
  default     = "15.0.0.0/29"
}

#DATOS PARA EL AKS
variable "vm_user_name" {
  description = "User name for the VM"
  default     = "vmuser1"
}
variable "public_ssh_key_path" {
  description = "Public key path for SSH."
  default     = "~/.ssh/id_rsa.pub"
}
variable "aks_agent_count" {
  description = "The number of agent nodes for the cluster."
  default     = 1
}
variable "aks_agent_vm_size" {
  description = "The size of the Virtual Machine."
  default     = "Standard_D2s_v3"
}
variable "aks_agent_os_disk_size" {
  description = "Disk size (in GB) to provision for each of the agent pool nodes. This value ranges from 0 to 1023. Specifying 0 applies the default disk size for that agentVMSize."
  default     = 40
}
variable "aks_dns_service_ip" {
  description = "Containers DNS server IP address."
  default     = "15.1.0.10"
}
variable "aks_docker_bridge_cidr" {
  description = "A CIDR notation IP for Docker bridge."
  default     = "172.17.0.1/16"
}
variable "aks_service_cidr" {
  description = "A CIDR notation IP range from which to assign service cluster IPs."
  default     = "15.1.0.0/16"
}
variable "aks_pod_cidr" {
  description = "A Pod CIDR notation IP range from which to assign service cluster IPs."
  default     = "15.244.0.0/16"
}