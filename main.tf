
#===============================================================================================
# INICIO - DATOS DE LA SUSCRIPCION
#===============================================================================================
provider "azurerm" {
  version = ">2.0.0"  
  subscription_id = "${var.subscription_id}"
  client_id       = "${var.client_id}"
  client_secret   = "${var.client_secret}"
  tenant_id       = "${var.tenant_id}"
  features {}
}
#===============================================================================================
# FIN - DATOS DE LA SUSCRIPCION
#===============================================================================================



#===============================================================================================
# INICIO - CREACION DEL RESOURCES GROUP
#===============================================================================================
resource "azurerm_resource_group" "siniestros" {
  name     = "${var.prefix}-resources"
  location = "${var.azure_location}"
}
#===============================================================================================
# FIN - CREACION DEL RESOURCES GROUP
#===============================================================================================



#===============================================================================================
# INICIO - CREACION DEL API MANAGMENT
#===============================================================================================
resource "azurerm_api_management" "siniestro" {
  name                = "${var.prefix}apimgmt"
  location            = "${azurerm_resource_group.siniestros.location}"
  resource_group_name = "${azurerm_resource_group.siniestros.name}"
  publisher_name      = "Siniestros"
  publisher_email     = "diegosoteloestupinan24@gmail.com"

  sku_name = "Developer_1"

  policy {
    xml_content = <<XML
    <policies>
      <inbound />
      <backend />
      <outbound />
      <on-error />
    </policies>
XML

  }
}
#===============================================================================================
# FIN - CREACION DEL API MANAGMENT
#===============================================================================================



#===============================================================================================
# INICIO - CREACION DE BD COSMOS
#===============================================================================================
resource "azurerm_cosmosdb_account" "siniestro" {
  name                = "${var.prefix}-cosmosdb"
  location            = "${azurerm_resource_group.siniestros.location}"
  resource_group_name = "${azurerm_resource_group.siniestros.name}"
  offer_type          = "Standard"
  kind                = "GlobalDocumentDB"

  consistency_policy {
    consistency_level       = "Session"
    max_interval_in_seconds = 5
    max_staleness_prefix    = 100
  }

  geo_location {
    prefix            = "${var.prefix}-customid"
    location          = "${azurerm_resource_group.siniestros.location}"
    failover_priority = 0
  }
}
#===============================================================================================
# FIN - CREACION DE BD COSMOS
#===============================================================================================

#===============================================================================================
# INICIO - CREACION DEL CONTAINER REGISTRY
#===============================================================================================
resource "azurerm_container_registry" "acr" {
  name                     = "${var.prefix}acr"
  resource_group_name      = "${azurerm_resource_group.siniestros.name}"
  location                 = "${azurerm_resource_group.siniestros.location}"
  sku                      = "Basic"
  admin_enabled            = true
}
#===============================================================================================
# FIN - CREACION DEL CONTAINER REGISTRY
#===============================================================================================



#===============================================================================================
# INICIO - CREACION DE STORAGEACCOUNT
#===============================================================================================
resource "azurerm_storage_account" "siniestro" {
  name                     = "${var.prefix}storageacc"
  resource_group_name      = "${azurerm_resource_group.siniestros.name}"
  location                 = "${azurerm_resource_group.siniestros.location}"
  account_tier             = "Standard"
  account_replication_type = "LRS"
}

resource "azurerm_storage_container" "siniestro" {
  name                  = "${var.prefix}container"
  storage_account_name  = "${azurerm_storage_account.siniestro.name}"
  container_access_type = "private"
}
#===============================================================================================
# FIN - CREACION DE STORAGEACCOUNT
#===============================================================================================



#===============================================================================================
# INICIO - CREACION DE VIRTUAL NETWORK Y SUBNETS
#===============================================================================================
resource "azurerm_virtual_network" "test" {
  name                = "${var.virtual_network_name}"
  location            = "${azurerm_resource_group.siniestros.location}"
  resource_group_name = "${azurerm_resource_group.siniestros.name}"
  address_space       = ["${var.virtual_network_address_prefix}"]

  subnet {
    name           = "${var.aks_subnet_name}"
    address_prefix = "${var.aks_subnet_address_prefix}"
  }

  subnet {
    name           = "${var.aks_subnetGW_name}"
    address_prefix = "${var.app_gateway_subnet_address_prefix}"
  }

  tags = {
    Environment = "Developer"
  }
}

data "azurerm_subnet" "kubesubnet" {
  name                 = "${var.aks_subnet_name}"
  virtual_network_name = "${azurerm_virtual_network.test.name}"
  resource_group_name  = "${azurerm_resource_group.siniestros.name}"
}
#===============================================================================================
# FIN - CREACION DE VIRTUAL NETWORK Y SUBNETS
#===============================================================================================



#===============================================================================================
# INICIO - CREACION DE AKS
#===============================================================================================
resource "azurerm_kubernetes_cluster" "aks" {
  name                = "${var.prefix}aks"
  location            = "${azurerm_resource_group.siniestros.location}"
  resource_group_name = "${azurerm_resource_group.siniestros.name}"
  dns_prefix          = "${var.prefix}dns"

  linux_profile {
    admin_username = "${var.vm_user_name}"

    ssh_key {
      #key_data = "${file("${var.public_ssh_key_path}")}"
      key_data = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDrtzXhFi7lnkfD8nw1GIP/9+qPBWEgY13jyOH/Si5wa9no36+sk0hpE8Khwv+v8E3h23F12RoUvyhDH7CbI7TN6cfyUxWwRofBz0SE68Y7F+bOEG9Qt7eDiTgN+Tk8Ltd7oOB2aTeNkSGdKaL2iUt5wklp5N3vaDuoMr0UCSPujbTGaNBUCNwLLti3fLg79KAkimXmWe4o41sgSBG9ikUhXaQKB3k70pM3cSFnWqCqrZOTMLP/6lYUCP1AnNyxnd6r1QEfMe7fC7fvuIu6TIhvkRMA5U7JMD3TDnZGhKdayFtatXailPFRWb1+yjIXobRvEnRQW9Q8BI4dkFWZ5y1j"
    }
  }

  addon_profile {
    http_application_routing {
      enabled = false
    }
  }

  default_node_pool {
    name       = "nodepool1"
    node_count = "${var.aks_agent_count}"
    vm_size    = "${var.aks_agent_vm_size}"
    os_disk_size_gb = "${var.aks_agent_os_disk_size}"    
    vnet_subnet_id  = "${data.azurerm_subnet.kubesubnet.id}"
  }

  service_principal {
    client_id     = "${var.client_id}"
    client_secret = "${var.client_secret}"
  }

  network_profile {
    network_plugin     = "azure"
  }
  
  depends_on = ["azurerm_virtual_network.test"]

  tags = {
    Environment = "Developer"
  }
}
#===============================================================================================
# FIN - CREACION DE AKS
#===============================================================================================
